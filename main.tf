variable "node_count" {
  default = "3"
}

resource "google_compute_instance" "vm_instance" {

  count        = var.node_count
  name         = "my-inst${count.index + 1}"
  machine_type = "e2-small" 
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = "20"
    }
  }

  connection {
    user       = "karinakitty555"
    private_key = "${file(var.private_key_path)}"
    timeout    = "2m"
    host       = "${self.network_interface.0.access_config.0.nat_ip}"
  }

  provisioner "local-exec" {
    command = "echo ${self.name} ${self.network_interface.0.access_config.0.nat_ip} >> host.list"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sudo apt install -y nginx",
      "echo Juneway ${self.network_interface.0.network_ip} ${self.name} | sudo tee /var/www/html/index.html",
      "sudo nginx -s reload"
    ]
  }
  
  network_interface {
    network = "default"
    access_config {

    }
  }
}
